// 解决第三方库依赖
const optimization = {
    splitChunks: {
        cacheGroups: {
            vendor: {
                test: /node_modules/,
                chunks: 'initial',
                name: 'vendor',
                priority: 10,
                enforce: true
            }
        }
    },
    // 运行时
    runtimeChunk: {
        name: 'runtime'
    }
}

module.exports = optimization