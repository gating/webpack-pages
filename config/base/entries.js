const glob = require('glob')
// 获取所有入口文件
const getEntry = function (globPath) {
    let entries = {};
    glob.sync(globPath).forEach(function (entry) {
        var pathname = entry.split('/').splice(-1).join('/').split('.')[0];
        entries[pathname] = [entry];
    });
    return entries;
};
const entries = getEntry('./src/static/js/*.js')

module.exports = entries