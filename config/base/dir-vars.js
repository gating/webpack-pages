const path = require('path')
const dir = {}
// 项目根目录
dir.staticRootDir = path.resolve(__dirname, '../../')
// 项目相关业务代码
dir.srcRootDir = path.resolve(dir.staticRootDir, './src');
// 不需要修改的第三方库
dir.vendorDir = path.resolve(dir.staticRootDir, './vendor');

module.exports = dir