const {
    staticRootDir
} = require('./base/dir-vars')

const path = require('path')

const output = {
    // 必须是绝对路劲
    path: path.resolve(staticRootDir, 'dist'),
    filename: 'js/[name].js'
}

module.exports = output