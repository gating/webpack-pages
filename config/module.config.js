const path = require('path')
const ExtractTextPlugin = require('extract-text-webpack-plugin')


const {
    staticRootDir
} = require('./base/dir-vars')

const loaders = {
    rules: [{
            test: /\.ejs$/,
            use: ['ejs-loader']
        }, {
            test: /\.js$/,
            use: ['babel-loader'],
            include: [
                path.resolve(staticRootDir, 'src/static/js')
            ]
        },
        {
            test: /\.css$/,
            // 从右向左(从下向上)开始执行
            // 提取css
            use: ExtractTextPlugin.extract({
                // 只在开发环境使用
                // use style-loader in development
                fallback: 'style-loader',
                use: ['css-loader', 'postcss-loader'],
                // 解决css打包背景图的路径问题
                publicPath: '../'
            })
        }, {
            test: /\.less$/,
            // use:['style-loader','css-loader','less-loader']
            // 如果想分离less
            use: ExtractTextPlugin.extract({
                // 只在开发环境使用
                // use style-loader in development
                fallback: 'style-loader',
                use: ['css-loader', 'postcss-loader', 'less-loader'],
                // 解决css打包背景图的路径问题
                publicPath: '../'
            })
        },
        {
            test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
            use: [{
                loader: 'url-loader',
                options: {
                    // 小于10kb转成base64
                    limit: 1024,
                    // 打包后的文件夹
                    outputPath: 'images'
                }
            }, {

                loader: 'image-webpack-loader',
                options: {
                    mozjpeg: {
                        progressive: true,
                        quality: 65
                    },
                    // optipng.enabled: false will disable optipng
                    optipng: {
                        enabled: false,
                    },
                    pngquant: {
                        quality: '65-90',
                        speed: 4
                    },
                    gifsicle: {
                        interlaced: false,
                    },
                    // the webp option will enable WEBP
                    webp: {
                        quality: 75
                    }
                }
            }]
        }
    ]
}

module.exports = loaders