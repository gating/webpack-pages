const path = require('path')
const {
    staticRootDir
} = require('./base/dir-vars')
// 生成html页面
const HtmlWebpackPlugin = require('html-webpack-plugin')
// 删除某些东西
const CleanWebpackPlugin = require('clean-webpack-plugin')
// 处理静态资源,静态资源输出
const CopyWebpackPlugin = require('copy-webpack-plugin')
const webpack = require('webpack')
// 提取css
const ExtractTextPlugin = require('extract-text-webpack-plugin')
// 删除冗余css代码
const PurifyCssWebpack = require('purifycss-webpack')
const glob = require('glob')
// 解决css压缩
const OptimizeCssnanoPlugin = require('@intervolga/optimize-cssnano-plugin');
// 自动引入静态资源
var HtmlWebpackIncludeAssetsPlugin = require('html-webpack-include-assets-plugin');



const chunks = require('./base/pageChunks')

const metaArr = [{
    charset: 'UTF-8'
}, {
    name: 'viewport',
    content: 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'
}, {
    'http-equiv': 'X-UA-Compatible',
    content: 'ie=edge'
}, {
    name: 'keywords',
    content: '波浪智投,外汇跟单,外汇交易平台,外汇跟单社区,外汇跟单系统,外汇代理合作机构'
}, {
    name: 'description',
    content: '波浪智投是一款致力于为用户提供可靠的外汇跟单平台的智能投顾软件,提供外汇跟单系统,外汇交易入门,外汇分析软件,外汇模拟交易平台,外汇策略,外汇理财,外汇交易高手'
}]

const plugins = [
    // 删除的目录
    new CleanWebpackPlugin(['dist']),
    new webpack.DllReferencePlugin({
        context: staticRootDir,
        manifest: require('../manifest.json'),
        name: 'comment',
    }),
    // 提取css
    new ExtractTextPlugin({
        filename: 'css/[name].css',
        // 根据不同环境走不同的配置（在开发环境下不使用）
        // disable: process.env.NODE_ENV === "development"
    }),
    // 静态资源输出
    // new CopyWebpackPlugin([{
    //     // 初始文件夹
    //     from: path.resolve(staticRootDir, 'vendor/atricle'),
    //     // 目标文件夹
    //     to: 'js'
    // }]),
    // 删除冗余css代码
    new PurifyCssWebpack({
        paths: glob.sync(path.join(staticRootDir, 'src/pages/**/*.ejs'))
    }),
    // 使用热更新
    new webpack.HotModuleReplacementPlugin(),
    // 向全局暴露第三方库
    new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.$': 'jquery',
        'window.jQuery': 'jquery'
    }),
    new OptimizeCssnanoPlugin({
        // sourceMap: nextSourceMap,
        cssnanoOptions: {
            preset: ['default', {
                discardComments: {
                    removeAll: true,
                },
            }],
        },
    })
]

// 生成HTML文件
chunks.forEach(function (pathname) {
    if (pathname == 'vendor') {
        return;
    }
    var conf = {
        // 输出的文件名
        filename: pathname + '.html',
        // 引用的模板文件
        template: path.resolve(staticRootDir, './src/pages/' + pathname + '/' + pathname + '.js'),
        // 是否清除缓存
        hash: true,
        // 页面的js
        chunks: [pathname, 'runtime', 'vendor'],
        // 将脚本放在head元素中,默认为body中
        // inject: 'head',
        meta: metaArr,
        // 压缩html
        minify: {
            // 删除空白字符(折叠空白区域)
            collapseWhitespace: true,
            // 删除属性的双引号
            removeAttributeQuotes: true
        },
        // 站点图标
        favicon: path.resolve(staticRootDir, './src/wave.ico')
    };
    plugins.push(new HtmlWebpackPlugin(conf));
});


plugins.push(new HtmlWebpackIncludeAssetsPlugin({
    assets: ['js/comment.js', 'css/comment.css'],
    // files: ['index.html'],
    append: false,
    hash: true
}))


module.exports = plugins