const {
    staticRootDir
} = require('./base/dir-vars')

const path = require('path')

const devServer = {
    // 设置服务器访问的基本目录
    contentBase: path.resolve(staticRootDir, 'dist'),
    // 设置开发服务器的地址
    host: 'localhost',
    // 设置开发服务器的端口
    port: 8080,
    // 自动打开浏览器
    open: true,
    // 配置热更新
    hot: true
}
module.exports = devServer