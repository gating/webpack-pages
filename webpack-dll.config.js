const {
    staticRootDir
} = require('./config/base/dir-vars')
const path = require('path')
const webpack = require('webpack')
// 文件处理器
const loaders = require('./config/module.config')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
// 解决css压缩
const OptimizeCssnanoPlugin = require('@intervolga/optimize-cssnano-plugin');


module.exports = {
    // 入口配置
    entry: {
        comment: ['jquery', './vendor/css/comment.css'],
    },
    // 出口文件
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'js/[name].js',
        library: '[name]'
    },
    // module.rules
    // loaders
    module: loaders,
    // 插件
    plugins: [
        new webpack.DllPlugin({
            path: path.join(staticRootDir, './manifest.json'),
            name: '[name]',
            context: staticRootDir,
        }),
        // 向全局暴露第三方库
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.$': 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ExtractTextPlugin('css/[name].css'),
        new OptimizeCssnanoPlugin({
            // sourceMap: nextSourceMap,
            cssnanoOptions: {
                preset: ['default', {
                    discardComments: {
                        removeAll: true,
                    },
                }],
            },
        })
    ],
}