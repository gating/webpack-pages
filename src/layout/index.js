const layout = require('./layout.ejs');
const header = require('./header.ejs');
const footer = require('./footer.ejs');

const pf = {
    pageTitle: '',
    navName: ''
};

const moduleExports = {
    init({
        pageTitle,
        navName
    }) {
        pf.pageTitle = pageTitle;
        pf.navName = navName;
        return this;
    },
    run(content) {
        const componentRenderData = Object.assign({}, {
            navList: [{
                title: '首页',
                url: 'index'
            }, {
                title: '关于我们',
                url: 'about'
            }]
        }, pf);
        const renderData = {
            header: header(componentRenderData),
            footer: footer(componentRenderData),
            content,
        };
        return layout(renderData);
    },
};

module.exports = moduleExports;