const content = require('./index.ejs');
const layout = require('../../layout');
module.exports = layout.init({
    pageTitle: '首页',
    navName: 'index'
}).run(content());