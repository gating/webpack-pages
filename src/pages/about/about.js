const content = require('./about.ejs');
const layout = require('../../layout');
module.exports = layout.init({
    pageTitle: '关于我们',
    navName: 'about'
}).run(content());