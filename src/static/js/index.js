import '../css/index.less'
import '../../../vendor/css/bootstrap.min.css'


var a = {
    b: 1
}

Object.assign(a, {
    a: 2
})

$('body').on('click', function () {
    alert(1)
})

function pro() {
    return new Promise((resolve, rejects) => {
        setTimeout(() => {
            resolve(a)
        }, 1000)
    })
}

pro().then(res => {
    console.log(res);
})