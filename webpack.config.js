// 入口文件
const entry = require('./config/base/entries');
// 出口文件
const output = require('./config/output')
// 文件处理器
const loaders = require('./config/module.config')
// 插件
const plugins = require('./config/plugins.config')
// 开发服务器
const devServer = require('./config/devServer')
// 解决第三方库依赖
const optimization = require('./config/optimization')

module.exports = {
	// 入口配置
	entry,
	// 出口文件
	output,
	// module.rules
	// loaders
	module: loaders,
	// 插件
	plugins,
	// 开发服务器
	devServer,
	// 解决第三方库依赖
	optimization
	// 开启调试模式，上线后要注释
	// webpack3.x之前开启的，webpack4用development
	// devtool: 'source-map'
}