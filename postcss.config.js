module.exports = {
    plugins: [
        // browsers模式选择：
        // 主流浏览器最近2个版本用 last 2 versions 
        // 全球统计有超过1 % 的使用率使用 > 1%
        // 仅新版本用 ff > 20 或 ff>=20 .
        // cascade 是否美化属性值(默认为true)
        require('autoprefixer')({
            browsers: ['last 4 versions', 'IE 10', 'ff > 10'],
            cascade: true
        })
        // require('cssnano')({
        //     preset: 'default'
        // })
    ]
};